﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tic_Tac_Toe
{
    public partial class MainPage : ContentPage
    {
        Random num = new Random();

        public MainPage()
        {
            InitializeComponent();
        }

        void OnClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

        //   Two Player Game    //
            if (TurnOf.Text == "O" && btn.Text == "")
            {
                btn.Text = TurnOf.Text;
                TurnOf.Text = "X";
            }
            else if (TurnOf.Text == "X" && btn.Text == "")
            {
                btn.Text = TurnOf.Text;
                TurnOf.Text = "O";
            }

            if (Button1.Text == "X" && Button2.Text == "X" && Button3.Text == "X" || Button1.Text == "O" && Button2.Text == "O" && Button3.Text == "O")
            {
                Button1.TextColor = Color.Teal;
                Button1.BackgroundColor = Color.Black;
                Button2.TextColor = Color.Teal;
                Button2.BackgroundColor = Color.Black;
                Button3.TextColor = Color.Teal;
                Button3.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button4.Text == "X" && Button5.Text == "X" && Button6.Text == "X" || Button4.Text == "O" && Button5.Text == "O" && Button6.Text == "O")
            {
                Button4.TextColor = Color.Teal;
                Button4.BackgroundColor = Color.Black;
                Button5.TextColor = Color.Teal;
                Button5.BackgroundColor = Color.Black;
                Button6.TextColor = Color.Teal;
                Button6.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button7.Text == "X" && Button8.Text == "X" && Button9.Text == "X" || Button7.Text == "O" && Button8.Text == "O" && Button9.Text == "O")
            {
                Button7.TextColor = Color.Teal;
                Button7.BackgroundColor = Color.Black;
                Button8.TextColor = Color.Teal;
                Button8.BackgroundColor = Color.Black;
                Button9.TextColor = Color.Teal;
                Button9.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button1.Text == "X" && Button4.Text == "X" && Button7.Text == "X" || Button1.Text == "O" && Button4.Text == "O" && Button7.Text == "O")
            {
                Button1.TextColor = Color.Teal;
                Button1.BackgroundColor = Color.Black;
                Button4.TextColor = Color.Teal;
                Button4.BackgroundColor = Color.Black;
                Button7.TextColor = Color.Teal;
                Button7.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button2.Text == "X" && Button5.Text == "X" && Button8.Text == "X" || Button2.Text == "O" && Button5.Text == "O" && Button8.Text == "O")
            {
                Button2.TextColor = Color.Teal;
                Button2.BackgroundColor = Color.Black;
                Button5.TextColor = Color.Teal;
                Button5.BackgroundColor = Color.Black;
                Button8.TextColor = Color.Teal;
                Button8.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button3.Text == "X" && Button6.Text == "X" && Button9.Text == "X" || Button3.Text == "O" && Button6.Text == "O" && Button9.Text == "O")
            {
                Button3.TextColor = Color.Teal;
                Button3.BackgroundColor = Color.Black;
                Button6.TextColor = Color.Teal;
                Button6.BackgroundColor = Color.Black;
                Button9.TextColor = Color.Teal;
                Button9.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button1.Text == "X" && Button5.Text == "X" && Button9.Text == "X" || Button1.Text == "O" && Button5.Text == "O" && Button9.Text == "O")
            {
                Button1.TextColor = Color.Teal;
                Button1.BackgroundColor = Color.Black;
                Button5.TextColor = Color.Teal;
                Button5.BackgroundColor = Color.Black;
                Button9.TextColor = Color.Teal;
                Button9.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            else if (Button3.Text == "X" && Button5.Text == "X" && Button7.Text == "X" || Button3.Text == "O" && Button5.Text == "O" && Button7.Text == "O")
            {
                Button3.TextColor = Color.Teal;
                Button3.BackgroundColor = Color.Black;
                Button5.TextColor = Color.Teal;
                Button5.BackgroundColor = Color.Black;
                Button7.TextColor = Color.Teal;
                Button7.BackgroundColor = Color.Black;
                TurnOf.Text = "";
            }
            //   One Player Game    //
            if (Players.Text == "1-Player")
            {
                //Game Tree - Beginning Move = Corner - Move Second
                if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 3))
                    {
                        case 1:
                            Button5.Text = "O";
                            break;
                        case 2:
                            Button9.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button5.Text = "O";
                            break;
                        case 2:
                            Button7.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button5.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button5.Text = "O";
                            break;
                        case 2:
                            Button1.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                //Game Tree - Beginning Move = Side - Move Second
                else if (Button1.Text == string.Empty && Button2.Text == "X" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 5))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button8.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 5))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button7.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button6.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 5))
                    {
                        case 1:
                            Button3.Text = "O";
                            break;
                        case 2:
                            Button9.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button4.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "X" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 5))
                    {
                        case 1:
                            Button7.Text = "O";
                            break;
                        case 2:
                            Button9.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button2.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                //Game Tree - Beginning Move = Middle - Move Second
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == "X" && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 5))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button7.Text = "O";
                            break;
                        case 4:
                            Button9.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                //Game Tree - Second Move: Button1 (and mirrors) - Move First
                else if (Button1.Text == "X" && Button2.Text == "O" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button4.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button6.Text = "O";
                            break;
                        case 4:
                            Button7.Text = "O";
                            break;
                        case 5:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button2.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button7.Text = "O";
                            break;
                        case 5:
                            Button8.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "O" && Button9.Text == "X")
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                        case 4:
                            Button5.Text = "O";
                            break;
                        case 5:
                            Button6.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button2.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button8.Text = "O";
                            break;
                        case 5:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button2.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button8.Text = "O";
                            break;
                        case 5:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == "O" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                        case 4:
                            Button5.Text = "O";
                            break;
                        case 5:
                            Button6.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button2.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button5.Text = "O";
                            break;
                        case 4:
                            Button8.Text = "O";
                            break;
                        case 5:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "O" && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 6))
                    {
                        case 1:
                            Button4.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button6.Text = "O";
                            break;
                        case 4:
                            Button7.Text = "O";
                            break;
                        case 5:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == "O" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 3))
                    {
                        case 0:
                            Button6.Text = "O";
                            break;
                        case 1:
                            Button7.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "O")
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button1.Text = "O";
                            break;
                        case 1:
                            Button8.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button6.Text = "O";
                            break;
                        case 1:
                            Button7.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "O" && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button3.Text = "O";
                            break;
                        case 1:
                            Button4.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "O" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button2.Text = "O";
                            break;
                        case 1:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "O" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button3.Text = "O";
                            break;
                        case 1:
                            Button8.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "O" && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button4.Text = "O";
                            break;
                        case 1:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "O" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button2.Text = "O";
                            break;
                        case 1:
                            Button7.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "O" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == "O")
                {
                    switch (num.Next(1, 3))
                    {
                        case 0:
                            Button1.Text = "O";
                            break;
                        case 1:
                            Button6.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button3.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "O" && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    Button7.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "O" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button1.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "X" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "O" && Button9.Text == string.Empty)
                {
                    Button7.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "X" && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button1.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "O" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "X")
                {
                    Button3.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == "X" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 8))
                    {
                        case 1:
                            Button2.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                        case 4:
                            Button6.Text = "O";
                            break;
                        case 5:
                            Button7.Text = "O";
                            break;
                        case 6:
                            Button8.Text = "O";
                            break;
                        case 7:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    Button9.Text = "O";

                    TurnOf.Text = "X";
                }
                //Game Tree - Second Move: Button2 (and mirrors) - Move First
                else if (Button1.Text == "O" && Button2.Text == "X" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 4))
                    {
                        case 1:
                            Button4.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button7.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == "O" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button2.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "X" && Button9.Text == "O")
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button3.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button6.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "O" && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button9.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button8.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "X" && Button3.Text == "O" && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button6.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button9.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == "O")
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button7.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button8.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == "O" && Button8.Text == "X" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == "O" && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 4))
                    {
                        case 1:
                            Button2.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                        case 3:
                            Button3.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "X" && Button3.Text == string.Empty && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 3))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "O" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button3.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == string.Empty && Button8.Text == "X" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button9.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "O" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button7.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "O" && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "X" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "O" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button3.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == "O" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button9.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "O" && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "X" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 3))
                    {
                        case 1:
                            Button7.Text = "O";
                            break;
                        case 2:
                            Button5.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == "X" && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == "O" && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch(num.Next(1, 7))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                        case 4:
                            Button6.Text = "O";
                            break;
                        case 5:
                            Button7.Text = "O";
                            break;
                        case 6:
                            Button8.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == "O" && Button6.Text == "X" && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 7))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button2.Text = "O";
                            break;
                        case 3:
                            Button3.Text = "O";
                            break;
                        case 4:
                            Button7.Text = "O";
                            break;
                        case 5:
                            Button8.Text = "O";
                            break;
                        case 6:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == "O" && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == "X" && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 7))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button3.Text = "O";
                            break;
                        case 3:
                            Button4.Text = "O";
                            break;
                        case 4:
                            Button6.Text = "O";
                            break;
                        case 5:
                            Button7.Text = "O";
                            break;
                        case 6:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == "X" && Button5.Text == "O" && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {
                    switch (num.Next(1, 7))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button2.Text = "O";
                            break;
                        case 3:
                            Button3.Text = "O";
                            break;
                        case 4:
                            Button7.Text = "O";
                            break;
                        case 5:
                            Button8.Text = "O";
                            break;
                        case 6:
                            Button9.Text = "O";
                            break;
                    }

                    TurnOf.Text = "X";
                }
                //Game Tree - 
                else if (Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty)
                {


                    TurnOf.Text = "X";
                }
                //Game Tree - Second Move: Button1 (and mirrors) - Move First

            }

            //Button1.Text == string.Empty && Button2.Text == string.Empty && Button3.Text == string.Empty && Button4.Text == string.Empty && Button5.Text == string.Empty && Button6.Text == string.Empty && Button7.Text == string.Empty && Button8.Text == string.Empty && Button9.Text == string.Empty

            if (Button1.Text.Length == 1 && Button2.Text.Length == 1 && Button3.Text.Length == 1 && Button4.Text.Length == 1 && Button5.Text.Length == 1 && Button6.Text.Length == 1 && Button7.Text.Length == 1 && Button8.Text.Length == 1 && Button9.Text.Length == 1)
            {
                TurnOf.Text = "Draw";
            }
        }

        void ClickReset(object sender, EventArgs e)
        {
            StartReset.Text = "Reset";

            Button1.Text = "";
            Button1.BackgroundColor = Color.White;
            Button1.TextColor = Color.Black;
            Button2.Text = "";
            Button2.BackgroundColor = Color.White;
            Button2.TextColor = Color.Black;
            Button3.Text = "";
            Button3.BackgroundColor = Color.White;
            Button3.TextColor = Color.Black;
            Button4.Text = "";
            Button4.BackgroundColor = Color.White;
            Button4.TextColor = Color.Black;
            Button5.Text = "";
            Button5.BackgroundColor = Color.White;
            Button5.TextColor = Color.Black;
            Button6.Text = "";
            Button6.BackgroundColor = Color.White;
            Button6.TextColor = Color.Black;
            Button7.Text = "";
            Button7.BackgroundColor = Color.White;
            Button7.TextColor = Color.Black;
            Button8.Text = "";
            Button8.BackgroundColor = Color.White;
            Button8.TextColor = Color.Black;
            Button9.Text = "";
            Button9.BackgroundColor = Color.White;
            Button9.TextColor = Color.Black;

            int turn = num.Next(1, 3);

            if (turn % 2 == 0)
            {
                TurnOf.Text = "O";
            }
            else
            {
                TurnOf.Text = "X";
            }

            if (Players.Text == "1-Player")
            {
                if (TurnOf.Text == "O")
                {
                    switch (num.Next(1, 10))
                    {
                        case 1:
                            Button1.Text = "O";
                            break;
                        case 2:
                            Button2.Text = "O";
                            break;
                        case 3:
                            Button3.Text = "O";
                            break;
                        case 4:
                            Button4.Text = "O";
                            break;
                        case 5:
                            Button5.Text = "O";
                            break;
                        case 6:
                            Button6.Text = "O";
                            break;
                        case 7:
                            Button7.Text = "O";
                            break;
                        case 8:
                            Button8.Text = "O";
                            break;
                        case 9:
                            Button9.Text = "O";
                            break;
                    }
                    TurnOf.Text = "X";
                }            
            }
        }

        void Click2P(object sender, EventArgs e)
        {
            if (Players.Text == "1-Player")
            {
                Players.Text = "2-Player";
            }
            else
            {
                Players.Text = "1-Player";
            }
        }
    }
}
